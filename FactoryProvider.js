'use strict';

var ResponseMessages = require('./responseMessages');


function FactoryProvider() {
	this.models = [];
}

FactoryProvider.prototype.get =function(model) {
	if (typeof this.models[model] == "undefined") {
		return {
			error: true,
			data: null,
			errorMessage: ResponseMessages.RESOURCE_NOT_DEFINED
		};
	}
	return this.models[model];
}

FactoryProvider.prototype.add = function(factoryName, factory) {
	this.models[factoryName] = factory;
}


module.exports = FactoryProvider;