# A easy to use Rest Service for your Node app

Extend your app with a easy to use rest api. 
You need a express.js server and a mongodb running. 

Use monogoose to define your models.


You only have to define your resources. routing is done automatically for you.

# Getting Started

## 1. Use mongoose to define your models

GroupModel.js

	var mongoose = require('mongoose');

	var Schema = mongoose.Schema;

	// the groups
	var GroupSchema = new Schema({
		name: String,
		children: [ String ],
		parent: String,
		values: [ String ]
	});
	 

	var GroupModel = mongoose.model('Group', GroupSchema);

	module.exports = GroupModel;


## 2. Create a factory for your model

You extend the ResourceFactory class from the rest-resource model. 

GroupFactory.js 

	var restResource = require('../../modules/mhaefner/rest-resource/rest-resource');
	var GroupModel = require('./GroupMongoose');

	var ResourceFactory = restResource.resourceFactory;

	function GroupFactory() {
		ResourceFactory.call(this, GroupModel);

		this.attributes = [
			"name", "children", "parent", "values"
		];
	};

	GroupFactory.prototype = Object.create(ResourceFactory.prototype); // See note below
	GroupFactory.prototype.constructor = GroupFactory;

	// validate model attribures
	GroupFactory.prototype.validate = function(model) {
		if ( model.name == "Linksammlung") {
			return true;
		}
		if ( typeof model.name != "string") return false;
		if ( typeof model.parent != "string") return false;

		return true;
	}

	module.exports = GroupFactory;


## 3. Add your factories to the FactoryProvider and let the RouterCreator create your routes

	var restResource = require('../modules/mhaefner/rest-resource/rest-resource');


	var groupFactory = require('./group/GroupFactory');
	var linkFactory = require('./link/LinkFactory');


	var factoryProvider = restResource.getFactoryProvider();

	factoryProvider.add("group", new groupFactory());
	factoryProvider.add("link", new linkFactory());


	var routerCreator = restResource.getRouterCreator();

	var router = routerCreator.create(factoryProvider);

	module.exports = router;


## 4. Append your routes to your express.js app

	app.use('/resource', resourceRoutes);

