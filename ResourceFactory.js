'use strict';

var q = require('q');
var ResponseMessages = require('./responseMessages');

function ResourceFactory(mongoose) {
	this.mongoose = mongoose;
	this.model = {};
};


ResourceFactory.prototype.validate = function(model) {
	return true;
};

// set model attribures
ResourceFactory.prototype.set = function(model) {

	var $this = this;

	this.attributes.forEach(function(attribute) {
		if ( typeof model[attribute] == "undefined") return;

		$this.model[attribute] = model[attribute];
	});
};

// get a resource
ResourceFactory.prototype.get = function(params) {

	var $this = this;

	var deferred = q.defer();
	this.mongoose.find(params, function(error, resource) {
		
		if (error) {
			deferred.reject(ResponseMessages["COULD_NOT_FIND"]);
		}

		$this.model = resource;

		deferred.resolve({
			error: false,
			data: resource
		});
	});

	return deferred.promise;
}


// get a resource by id
ResourceFactory.prototype.getById = function(id) {

	var $this = this;

	var deferred = q.defer();
	this.mongoose.findById(id, function(error, resource) {
		
		if (error) {
			deferred.reject(ResponseMessages["COULD_NOT_FIND"]);
		}

		$this.model = resource;

		deferred.resolve({
			error: false,
			data: resource
		});
	});

	return deferred.promise;
}

// search a resource
ResourceFactory.prototype.search = function(params) {

	var $this = this;

	var deferred = q.defer();
	this.mongoose.find(params, function(error, resource) {
		
		if (error) {
			deferred.reject(ResponseMessages["COULD_NOT_FIND"]);
		}

		$this.model = resource;

		deferred.resolve({
			error: false,
			data: resource
		});
	});

	return deferred.promise;
}

// save a resource
ResourceFactory.prototype.save = function(model) {

	console.log("SAVE");
	var deferred = q.defer();

	if (!this.validate(model)) {
		deferred.reject(ResponseMessages["RESOURCE_NOT_VALID"]);
		return deferred.promise;
	}

	this.set(model);

	this.model.save(function(error, resource) {
		if (error) {
			deferred.reject({
				error: true,
				errorMessage: ResponseMessages["COULD_NOT_SAVE_RESOURCE"],
				data: error
			});
		}

		deferred.resolve({
			error: false,
			data: resource
		});	
	});

	return deferred.promise;
};

// create a resource OBSOLETE
ResourceFactory.prototype.create = function(model) {

	console.log("create");
	console.log(model);
	var deferred = q.defer();

	// validation
	if (!this.validate(model)) {
		deferred.reject(ResponseMessages["RESOURCE_NOT_VALID"]);
		return deferred.promise;
	}

	// set model data
	this.model = new this.mongoose(model);

	// save model data
	this.model.save(function(error, resource) {
		if (error) {
			deferred.reject({
				error: true,
				errorMessage: ResponseMessages["COULD_NOT_SAVE_RESOURCE"],
				data: error
			});
		}

		deferred.resolve({
			error: false,
			data: resource
		});	
	});

	return deferred.promise;
};


// update a resource
ResourceFactory.prototype.update = function(id, model) {

	var deferred = q.defer();
	var $this = this;

	this.mongoose.findById(id, function(error, resource) {
		if (error) {
			deferred.reject(ResponseMessages["RESOURCE_NOT_DEFINED"]);
		}

		$this.model = resource;

		$this.save(model)
		.then(function(data) {
			deferred.resolve(data);
		})
		.catch(function(error) {
			deferred.reject(error);
		});
	});

	return deferred.promise;
};

// delete resource
ResourceFactory.prototype.delete = function(id) {

	var deferred = q.defer();

	this.mongoose.remove({ _id: id}, function(error, resource) {
		if (error) {
			deferred.reject(ResponseMessages["COULD_NOT_DELETE_RESOURCE"]);
		}

		deferred.resolve( {
			error: false,
			data: resource
		});	
	});

	return deferred.promise;
};


module.exports = ResourceFactory;