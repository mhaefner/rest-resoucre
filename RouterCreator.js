var express = require('express');
var url = require('url');
var bodyParser = require('body-parser');


function RouterCreator() {};

RouterCreator.prototype.create = function(factoryProvider) {

	var router = express.Router();

	router.use(bodyParser.json());


	// get all resources
	router.get('/:resource', function(request, response) {

		var Resource = factoryProvider.get(request.params.resource);
		
		// resource is not defined
		if (Resource.error) return Resource;

		// get the resource
		Resource.get(request.params.resource)
		.then(function(data) {
			response.send(data);
		})
		.catch(function(error) {
			response.send(error);
		});
	});
	// create new resource
	router.post('/:resource', function(request, response) {
		var Resource = factoryProvider.get(request.params.resource);
		
		// resource is not defined
		if (Resource.error) return Resource;

		// get the resource
		Resource.create(request.body)
		.then(function(data) {
			response.send(data);
		})
		.catch(function(error) {
			response.send(error);
		});
	});
	// search for resources
	router.post('/:resource/search', function(request, response) {
		var Resource = factoryProvider.get(request.params.resource);
		
		// resource is not defined
		if (Resource.error) return Resource;

		// get the resource
		Resource.search(request.body)
		.then(function(data) {
			response.send(data);
		})
		.catch(function(error) {
			response.send(error);
		});
	});


	// get by id
	router.get('/:resource/:r_id', function(request, response) {
		var Resource = factoryProvider.get(request.params.resource);
		
		// resource is not defined
		if (Resource.error) return Resource;

		Resource.getById(request.params.r_id)
		.then(function(data) {
			response.send(data);
		})
		.catch(function(error) {
			response.send(data);
		});

	});
	// update
	router.put('/:resource/:r_id', function(request, response) {
		var Resource = factoryProvider.get(request.params.resource);
		
		// resource is not defined
		if (Resource.error) return Resource;

		Resource.update(request.params.r_id, request.body)
		.then(function(data) {
			response.send(data);
		})
		.catch(function(error) {
			response.send(error);
		});
	});
	// delete 
	router.delete('/:resource/:r_id', function(request, response) {
		var Resource = factoryProvider.get(request.params.resource);
		
		// resource is not defined
		if (Resource.error) return Resource;

		Resource.delete(request.params.r_id)
		.then(function(data) {
			response.send(data);
		})
		.catch(function(error) {
			response.send(error);
		});
	});

	return router;
}


module.exports = RouterCreator;