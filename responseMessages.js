'use strict';

var ResponseMessages = {
	"RESOURCE_NOT_DEFINED": {
		error: true,
		data: null,
		errorMessage: 'Resource not defined'
	},
	"RESOURCE_NOT_FOUND": {
		error: true,
		data: null,
		errorMessage: 'Resource not found'
	},
	"RESOURCE_NOT_VALID": {
		error: true,
		data: null,
		errorMessage: 'Resource data not valid'
	},
	"COULD_NOT_SAVE_RESOURCE": {
		error: true,
		data: null,
		errorMessage: 'Could not save resource'
	},
	"COULD_NOT_DELETE_RESOURCE": {
		error: true,
		data: null,
		errorMessage: 'Could not delete resource'
	}
};

module.exports = ResponseMessages;