
var factoryProvider = require('./FactoryProvider');
var routerCreator = require('./RouterCreator');
var resourceFactory = require('./ResourceFactory');

var restResource = {
	factoryProvider: factoryProvider,

	getFactoryProvider: function() {
		return new factoryProvider();
	},

	routerCreator: routerCreator,

	getRouterCreator: function() {
		return new routerCreator();
	},

	resourceFactory: resourceFactory, 

	getResourceFactory: function() {
		return new resourceFactory();
	}
}

module.exports = restResource;